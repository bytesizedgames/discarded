# README #

This the repository for the game Discarded. It's a FPS multiplayer game being developed using the Unity Game engine for Windows, MacOSX and Linux.

## Implemented features ##

* Procedurally generated game world.

## Features being developed ##

* Multiplayer FPS combat.
* In game editor of player models/weapons/vehicles/items.